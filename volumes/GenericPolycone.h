/// @file GenericPolycone.h
/// @author Raman Sehgal (raman.sehgal@cern.ch)

#ifndef VECGEOM_VOLUMES_GENERICPOLYCONE_H_
#define VECGEOM_VOLUMES_GENERICPOLYCONE_H_

#include "base/Global.h"

#include "volumes/PlacedGenericPolycone.h"
#include "volumes/SpecializedGenericPolycone.h"
#include "volumes/UnplacedGenericPolycone.h"

#endif // VECGEOM_VOLUMES_GENERICPOLYCONE_H_
