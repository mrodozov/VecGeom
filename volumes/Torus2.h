/// @file Torus2.h
/// Includes all headers related to the torus volume.

#ifndef VECGEOM_VOLUMES_TORUS2_H_
#define VECGEOM_VOLUMES_TORUS2_H_

#include "base/Global.h"
#include "volumes/PlacedTorus2.h"
#include "volumes/SpecializedTorus2.h"
#include "volumes/UnplacedTorus2.h"

#endif // VECGEOM_VOLUMES_TORUS2_H_
