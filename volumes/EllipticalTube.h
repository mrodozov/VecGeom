/// @file EllipticalTube.h
/// @author Raman Sehgal (raman.sehgal@cern.ch), Evgueni Tcherniaev (evgueni.tcherniaev@cern.ch)

#ifndef VECGEOM_VOLUMES_ELLIPTICALTUBE_H_
#define VECGEOM_VOLUMES_ELLIPTICALTUBE_H_

#include "base/Global.h"

#include "volumes/PlacedEllipticalTube.h"
#include "volumes/SpecializedEllipticalTube.h"
#include "volumes/UnplacedEllipticalTube.h"

#endif // VECGEOM_VOLUMES_ELLIPTICALTUBE_H_
