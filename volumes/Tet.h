/// @file Tet.h
/// @author Raman Sehgal (raman.sehgal@cern.ch), Evgueni Tcherniaev (evgueni.tcherniaev@cern.ch)

#ifndef VECGEOM_VOLUMES_TET_H_
#define VECGEOM_VOLUMES_TET_H_

#include "base/Global.h"

#include "volumes/PlacedTet.h"
#include "volumes/SpecializedTet.h"
#include "volumes/UnplacedTet.h"

#endif // VECGEOM_VOLUMES_TET_H_
